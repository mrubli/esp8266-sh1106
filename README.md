# ESP8266 and SH1106 SPI OLED display

Sample code for interfacing with an SH1106-based OLED display connected to an
ESP8266 module using SPI.

This is the _SimpleDemo_ sample from the excellent
https://github.com/ThingPulse/esp8266-oled-ssd1306
library.

## Hardware

This uses a NodeMCU development kit with a noname 1.3" 128x64 monochrome OLED
module that looks like this:

![alt text](doc/oled_front.jpg)

![alt text](doc/oled_back.jpg)

## Wiring

On the left are the OLED module's pin labels, on the right the NodeMCU's:

```mermaid
graph LR;
  GND-->gnd[GND];
  VCC-->3V3;
  CLK-->D5("D5 (HSCLK)");
  MOSI-->D7("D7 (HMOSI)");
  DC-->D2;
  CS1-->D8("D8 (HCS)");
  FS0-->floating0("(floating)")
  CS2-->floating1("(floating)")
```
